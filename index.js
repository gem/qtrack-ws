const express = require('express')
const helmet = require('helmet')
const https = require('https')
const fs = require('fs')

const app = express()
app.use(helmet())

const port = process.env.PORT || 3000;
const options = {
  // in case we need to use the ssl: on line 3 change require('http') to require('https')
  // uncomment the 2 line below and set the certs path  
  key: fs.readFileSync("/etc/letsencrypt/live/socket.quickalgorithm.com/privkey.pem"),
  cert: fs.readFileSync("/etc/letsencrypt/live/socket.quickalgorithm.com/fullchain.pem")
}
const server = https.createServer(options, app)

const io = require('socket.io')({
  serveClient: false,
});

io.attach(server, {
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
});

io.on('connection', (socket) => {
  socket.emit('connected', { id: socket.id });

  socket.on('room', room => {
    socket.join(room)
    socket.emit(`connected to room: ${room}`, { id: socket.id });
  })

  socket.on('create', data => {
    const parsedData = JSON.parse(data)  
    const dataToSend = JSON.stringify(parsedData.data)
    io.to(parsedData.room).emit('create', dataToSend)
  })

  socket.on('update', data => {
    const parsedData = JSON.parse(data)  
    const dataToSend = JSON.stringify(parsedData.data)
    io.to(parsedData.room).emit('update', dataToSend)
  })

  socket.on('delete', data => {
    const parsedData = JSON.parse(data)  
    const dataToSend = JSON.stringify(parsedData.data)
    io.to(parsedData.room).emit('delete', dataToSend)
  })

  socket.on('alert_notification', data => {
    const parsedData = JSON.parse(data)
    const dataToSend = JSON.stringify(parsedData.data)
    io.to(parsedData.room).emit('alert_notification', dataToSend)
  })
});

//start our server

server.listen(port)